# What is this? #

This is a Python script that can be used in order to make trading Steam Trading Cards (STC) more easily. To work, it only needs your Steam Web API Key, your Steam ID 64 and a place to store a database.

You'll obviously also have to have Python installed on your computer to make it work.

# How do I begin? #

## Configuration ##

First of all, download the script by [clicking here](https://bitbucket.org/ThomasKowalski/steam-trading-cards-tools/get/HEAD.zip).

Extract it, and rename config.py to config_user.py. Open it using your favorite text editor (I recommend Notepad++).

* On the first line, between the brackets, paste the Steam ID 64 you can get here: 
* On the second line, between the brackets, paste the Steam Web API Key [you can get here](https://steamcommunity.com/dev/apikey).
* On the third, between the brackets, paste the name of the database file. The file needs not to already exist, and the folder containing it must exist. To place it on your Desktop, you can simply use C:/Users/YourName/Desktop/SteamTradingCardsToolsDatabase.s3db. The name of the file doesn't matter as long as it matches the config file.
* On the fourth line, add your Steam cookie. *This is not necessary, but it will allow you to gather the inventories of your friends whose inventories are friends-only.* To get it, using Chrome, go to [Cookies](chrome://settings/cookies) and search "store.steampowered.com". Click the right site to display all cookies, click the steamLogin one, and copy its content to this line, between the brackets.
* On the fifth line, select the currency you want to use, following this table (other exist, but I don't know them):
```
    USD : 1
    GBP : 2
    EUR : 3
    CHF : 4
    RUB : 5
    BRL : 7
    YEN : 8
    SEK : 9
    IDR : 10
    MYR : 11
    SGD : 13
    ........
```
    
Save the file. You're ready!

Note: you also have to make your profile public. 

## Using the script ##

The script, as of now, allows you to do only a few things.
First of all, you'll need to gather the information needed (friends' names, games and inventories). It of course works only if their public is public. To do this, add the following line to the file.

### Gathering necessary data ###

```
getData()
```

You can run the script now, so the information will be here and usable easily after. During the time, unless you don't have a lot of friends, you'll have to get you a coffee and drink it. 

### Exploiting the data ###

Now, let's have fun. First of all, remove the getData() bit. Otherwise, the script will get the data again each time you run the script. And you don't wanna do this.

The first use of this script is finding the duplicates in your friends' inventories. This is why the getDuplicates function is:

```

getDuplicates(Friends, Games, Recursive)

```

* Friends can be a string (e.g. "76561198107732466"), an integer (e.g. 76561198107732466) or a list (e.g. [76561198107732466, "76561198106537837"]). Use "*" to search in all inventories.
* Games can be a string (e.g. "730"), an integer (e.g. 730) or a list (e.g. [730, "218620"]). They represent application ids. More about this can be read at the bottom. Use "*" to search for items from all games.
* Recursive must always be False (with a capital F) when you call the function.

There is a second function looking like this: getCards(). getCards will simply be used to get the cards available in your friend's inventories. 

```

getCards(Friends, Games, Recursive)

```

* Friends can be a string (e.g. "76561198107732466"), an integer (e.g. 76561198107732466) or a list (e.g. [76561198107732466, "76561198106537837"]). Use "*" to search in all inventories.
* Games can be a string (e.g. "730"), an integer (e.g. 730) or a list (e.g. [730, "218620"]). They represent application ids. More about this can be read at the bottom. Use "*" to search for items from all games.
* Recursive must always be False (with a capital F) when you call the function.

Though, to do this, and if you're looking to craft badges, you can simply see the progress of your badge in your inventory, and see what friends have it.

### Filtering the results ###

You can then use other functions to filter the results. Let's say you want to see who has the "IDF" card from CS:GO, and has at least two of them. Use filterByGame to do this!

```

filterByName(CardsToFilter, Name)

```

* CardsToFilter is a dictionary. You should never build it yourself! This dictionary is returned by the two functions presented before.
* Name can be a string (e.g. "IDF") or a list (e.g. ["IDF", "FBI"]). It represents the names that a name can contain in order to be selected.

Let's say your CardsToFilter looks like this:

```
76561198106537837
    730-IDF
    218620-Bain
    730-FBI
76561198093021758
	440-ENGINEER (Trading Card)
    730-Anarchist
	1250-Clots
	440-SCOUT (Trading Card)
	440-HEAVY (Trading Card)
```

Applying filterByName(dictionary, ["FBI", "IDF"]) will return 

```
76561198106537837
    730-IDF
    730-FBI
76561198093021758
```

Easy, isn't it?

Note: there is also a filterByGame function that does pretty much the same, but instead of filtering by name, it filters the results by game. Since it's already integrated in the first functions, I won't cover them here. Though, its usage is pretty straight-forward.

### Displaying the results ###

Finally, you want to print the results, don't you? To do this, use printResults.

```

printResults(ResultsDictionary, PrintEmptySteamIDs)

```

* ResultsDictionary is the results you got by using the exploitation and filtering functions. 
* PrintEmptySteamIDs is a boolean (True, or False - with the capital F and T!) that says whether or not the script should display an ID in the console if it has not items that matched the criteria imposed by the functions before. Let's say ID 76561198106537837 is in your friends. If you decided to look for duplicates from CS:GO and he doesn't have any, there won't be any results for this person. If you wanna display their ID (with no data associated) in the console, pass a True parameter. Otherwise, just pass False.

### Other functions ###

To do some more complex work, you may want to get other data. To do this, a few functions are available.

```

findFriendsWithGames(Games, mustMatchAllGames)

```

* Games can be a string, an integer, or a list of mixed (string or integer). It represents what game(s) should the user have in order to be chosen.
* mustMatchAllGames is a boolean and represents whether or not all the games of the Games list must be present on the account. For example, if you want to find the friends who have both PAYDAY The Heist and CS:GO, you must set this to True. If you want to find friends who have PAYDAY The Heist or CS:GO (non exclusively), you must set it to False.

This function will return a list of Friend objects that match the search criteria. 

```

getBoosterPackPrice(Games)

```

* Games can be a string, an integer or a list or tuple of mixed (string or integer). It represents what game(s) you want to get booster pack prices for. *You have to own these games, othewise an exception will be raised!* Before using this function, you have to set your currency in the config_user.py file!

It returns a dictionary of Game -> Int. The Game object is defined at the top of the file and defines a game (a Name and an AppID).

Note: the results of this function are expressed in cents.

# Examples #

*You can find more complex examples in the examples.py file, including one using the findFriendsWithGames function.*

If everything here is not clear to you, just see examples. You'll quickly understand.

```


#Print duplicates in 76561198057827220 and 76561198059577879's inventories for CS:GO (AppId: 730)

printResults(getDuplicates([76561198059577879, 76561198057827220], 730, False), False) #First False is Recursive, second False is PrintEmptySteamIDs

# Returns
# 76561198057827220 (waddawa)
# 	730-FBI
# 	730-SWAT
# 	730-Anarchist
# 	730-Guarded
#
# Note: The second SteamID didn't have any duplicates.

```

```

#Print duplicates in 76561198057827220 and 76561198059577879's inventories for CS:GO (AppId: 730) whose name are IDF or FBI.

printResults(filterByName(getDuplicates([76561198059577879, 76561198057827220], 730, False), ["FBI", "IDF"]), False) #First False is Recursive, second False is PrintEmptySteamIDs

# Returns
# 76561198057827220 (waddawa)
# 	730-FBI
#
# Note: The second SteamID didn't have any duplicates, so no duplicates matching the search.

```

```

#Print the cards from all inventories for PAYDAY 2 (AppId: 218620)

printResults(getCards("*", 218620, False), False) #First False is Recursive, second False is PrintEmptySteamIDs

# Returns
# 76561198087496735 (Steve [40Keys])
# 	218620-Wolf (Profile Background)
# 76561198102087738 (chicken)
# 	218620-:Y:
# 	218620-:A:
# 	218620-:P:
# [...]
# And so on (a lot of people having PAYDAY 2, I'm not gonna write everything here...)
```

# FAQ #

## How do I get my Steam Web API Key? ##

Get it here: http://steamcommunity.com/dev/apikey

## How do I get the applications IDs? ##

Make a search on the Steam store, and then go to the app's store page. The URL shoud looke like this:

```
http://store.steampowered.com/app/218620/?xxx
```

Get the number after "/app/"

## How do I get my Steam ID 64? ##

Use a site such as [SteamID.co.uk](http://steamid.co.uk).

## It doesn't work. ##

And this is not a question. [Open an issue here.](https://bitbucket.org/ThomasKowalski/steamstats/issues/new)

# Issues and bugs, possible improvements #

## Issues ##
* In the current state, the script also gets the Steam Community Items, so backgrounds and emoticons (not only trading cards) will look to fix this.

## Improvements ##
* Create a console user-interface, that would be better than having to edit the script each time you want to do a search.
* Automatically create links for trade-offers
* JSON or XML output