import steamprofile

# EXAMPLES
# To start an example, just call the function associated.
# For instance, to try the first example, write (at the end of the file)
# Example1()
# Then, run the script using Python (3).
# You will find a description for each example, and the explanation of how it works in the functions' bodies.
# Don't forget that it's necessary to run getData() once before calling any example!

# A few appids
#   PAYDAY2:                 218620
#   Chivalry:                219640
#   Garry's Mod:             4000
#   2016 Picnic Summer Sale: 480730
#   CS:GO:                   730

######################################
#            EXAMPLE 1               #
######################################

def Example1():
    # I had a Chivalry: Medieval Warfare card, without having the game. I wanted to trade it for a card of a game I had. Here is how to do it (for Garry's mod).

    # First we retrieve the friends that have both games (Chivalry and Garry's mod : they'll be interested in my card and are likely to have Garry's mod card(s))
    friendsWithBothGames = steamprofile.findFriendsWithGames([219640, 4000], True)

    # Then, find friends who have duplicates of Garry's mod cards
    friendsWithDuplicates_ = steamprofile.getDuplicates("*", 4000, False)

    # Filter them to only get the ones who have "Face Posing", "Movie Maker", "Spammer" and "Melon Racer"
    friendsWithDuplicates = steamprofile.filterByName(friendsWithDuplicates_, ["Face Posing", "Movie Maker", "Spammer", "Melon Racer"])

    # Finally, we need to take only the ones who are also in the first list (i.e. the ones who also have Chivalry: Medieval Warfare)
    result = []
    for friend in friendsWithBothGames: #for each friend who has both games
        if friendsWithDuplicates[friend.ID] != []: #if he has duplicates of the cards we want
            result.append((friend.ID, friend.Name)) #append this friend to the result list (make a tuple containing his name and SteamID64)

    # Print the results
    if len(result) > 0:
        for friend in result:
            try:    
                print(str(friend[0]) + " " + friend[1])
            except:
                print(friend[0])
    else:
        print("No match found.")

######################################
#            EXAMPLE 2               #
######################################

def Example2():
    # During the 2016 Picnic Summer Sale, I lacked the "You had one job" card. Here's how to get the friends that have a duplicate of this card.
    # Note: the Picnic Summer Sale doesn't use the Steam app id but its own: 480730

    # Get the friends with duplicates
    friendsWithDuplicates_ = steamprofile.getDuplicates("*", 480730, False)
    # Filter the duplicates: take only the ones with "one job" in the name
    friendsWithDuplicates = steamprofile.filterByName(friendsWithDuplicates_, "one job")
    # Show the results
    steamprofile.printResults(friendsWithDuplicates, False)
    
######################################
#            EXAMPLE 3               #
######################################

def Example3():
    userGames = steamprofile.getGamesFromDatabase()
    results = steamprofile.getBoosterPackPrice([Game.ID for Game in userGames])
    for Game in results.keys():
        print(Game.Name + ": " + str(results[Game]))
        
Example3()