import basics
import config_user as config
import bs4 as BeautifulSoup
import json
import sqlite3
from datetime import datetime
import time
import collections
import os.path

class Game:
    ID = ''
    Name = ''

class Friend:
    ID = 0
    Games = []
    Since = 0
    TradingCards = []
    Name = ''
    
class Profile:
    ProfileName = ''
    LastLogOff = ''
    URL = ''
    RealName = ''

class TradingCard:
    ClassID = 0
    ID = 0
    InstanceID = 0
    Name = ''

class Description:
    Name = ''
    ClassID = 0
    
def getGames(ID):
    ID = str(ID)
    Games = []
    result = basics.downloadString("http://steamcommunity.com/profiles/" + ID + "/games/?tab=all", {})
    soup = BeautifulSoup.BeautifulSoup(result, "html.parser")
    links = soup.find_all('span', {"class" : "profile_small_header_name"})
    if links == []:
        Name = ''
    else:
        links = BeautifulSoup.BeautifulSoup(str(links[0]), "html.parser")
        for child in links.children:
            Name = child.string.strip()
    
    scripts = soup.find_all("script")
    
    rgGames = None
    
    for scriptTag in scripts:
        if "rgGames" in str(scriptTag):
            rgGames = str(scriptTag).split("\n")[1]
            rgGames = rgGames.strip().lstrip("\t").rstrip(";").replace("var rgGames = ", "")
    
    if rgGames == None:
        return [], Name

    rgGames = json.loads(rgGames)
    for game in rgGames:
        newGame = Game()
        newGame.ID = game["appid"]
        newGame.Name = game["name"]
        Games.append(newGame)
    return Games, Name
    
def getFriends(ID):
    Friends = []
    params = {'key' : config.steamapikey, 'steamid' : ID }
    result = basics.downloadString("http://api.steampowered.com/ISteamUser/GetFriendList/v0001/?", params)
    result = json.loads(result)
    if len(result.keys()) == 0:
        return -1
    for friend in result["friendslist"]["friends"]:
        newFriend = Friend()
        newFriend.ID = friend["steamid"]
        newFriend.Since = friend["friend_since"]
        Friends.append(newFriend)
    return Friends

def getTradingCards(ID):
    TradingCards = []
    result = None
    ID = str(ID)
    while result == None or str(result) == "null":
        result = basics.downloadString("http://steamcommunity.com/profiles/" + ID + "/inventory/json/753/6", {})
    resultJSON = json.loads(result)
    if resultJSON["success"] == False:
        return []
    descriptionsJSON = resultJSON["rgDescriptions"]
    Descriptions = []
    for desc in descriptionsJSON:
        newDescription = Description()
        newDescription.ClassID = descriptionsJSON[desc]["classid"]
        newDescription.Name = descriptionsJSON[desc]["market_hash_name"]
        Descriptions.append(newDescription)
    inventory = resultJSON["rgInventory"]
    for item in inventory:
        for desc in Descriptions:
            if desc.ClassID == inventory[item]["classid"]:
                TradingCards.append(desc.Name)
    return TradingCards
    
def getData(): 
    Friends = getFriends(config.steamid64)
    if Friends == -1:
        print("Your profile is private or friends-only. In order to use this script, you must set your privacy settings to Public.")
        return
    Games, Name = getGames(config.steamid64)
    TradingCards = getTradingCards(config.steamid64)
    
    for friend in Friends:
        friend.Games, friend.Name = getGames(friend.ID)
        friend.TradingCards = getTradingCards(friend.ID)
        
    db, cur = basics.openDatabase(config.databasepath)
    basics.createTable("Games", ["AppID", "Name"], ["INT", "TEXT"], cur)
    basics.createTable("Friends", ["SteamID", "Name", "Games", "TradingCards"], ["INT", "TEXT", "TEXT", "TEXT"], cur)
    for game in Games:
        basics.addLine("Games", [game.ID, game.Name], cur)
    for friend in Friends:
        basics.addLine("Friends", [friend.ID, friend.Name, str([game.ID for game in friend.Games]), str(friend.TradingCards)], cur)
    
    db.commit()     
    return Friends

def useDB():
    if config.databasepath == "":
        print("You haven't configured the script in config_user.py. Please do it before using any function.")
        return []
    if os.path.isfile(config.databasepath) == False:
        print("The database doesn't exist.")
        return []
    db, cur = basics.openDatabase(config.databasepath)
    Friends = []
    sql = "SELECT * FROM Friends"
    cur.execute(sql)
    test = cur.fetchall()
    for line in test:
        newFriend = Friend()
        newFriend.ID = line[0]
        newFriend.Name = line[1]
        newFriend.Games = json.loads(line[2])
        inv = line[3][1:][0:len(line[3]) - 2:1]
        if inv == '':
            newFriend.TradingCards = []
        else:
            newFriend.TradingCards = [x.strip()[1:len(x.strip()) - 1] for x in inv.split(",")]
        Friends.append(newFriend)
        
    dict = {}
    for friend in Friends:
        dict[friend.ID] = friend       
    return Friends, dict

def getFriendsAsList():
    return useDB()[0]

def getFriendsAsDictionary():
    return useDB()[1]
    
def filterByGame(cards, games):
    if games == "*":
        return cards
    returned = []
    if str(type(games)) == "<class 'int'>" or str(type(games)) == "<class 'str'>":
        games = [str(games)]
    for card in cards:
        cardGame = card.split("-")[0]
        if str(cardGame) in games:
            returned.append(card)
    return returned    

def filterByName(cards, names):
    if names == "*":
        return cards
    returned = {}
    if str(type(names)) == "<class 'int'>" or str(type(names)) == "<class 'str'>":
        names = [str(names)]
    for user in cards:
        returned[user] = []
        if len(cards[user]) > 0:
            for card in cards[user]:
                for name in names:
                    if name in str(card) and not card in returned[user]:
                        returned[user].append(card)
    return returned
  
def getCards(friend, games, recursive):
    returned = {}
    Friends = getFriendsAsList()
    if friend == "*":
        for friendDB in Friends:
            returned[friendDB.ID] = []
            tradingCards = filterByGame(friendDB.TradingCards, games)
            if len(tradingCards) > 0:
                for dup in tradingCards:
                    returned[friendDB.ID].append(dup)
    elif str(type(friend)) == "<class 'str'>" or str(type(friend)) == "<class 'int'>":   #specific friend
        for friendDB in Friends:
            if str(friend) == str(friendDB.ID):
                tradingCards = filterByGame(friendDB.TradingCards, games)
                returned[friendDB.ID] = []
                if len(tradingCards) > 0:
                    for dup in tradingCards:
                        returned[friendDB.ID].append(dup)
    else:
        for friend_ in friend:
            returned[friend_] = getCards(friend_, games, True)[friend_]
    return returned
    
def getDuplicates(friend, games, recursive):
    returned = {}
    Friends = getFriendsAsList()
    if friend == "*":
        for friendDB in Friends:
            returned[friendDB.ID] = []
            duplicates = [item for item, count in collections.Counter(friendDB.TradingCards).items() if count > 1]
            duplicates = filterByGame(duplicates, games)
            if len(duplicates) > 0:
                for dup in duplicates:
                    returned[friendDB.ID].append(dup)                  
    elif str(type(friend)) == "<class 'str'>" or str(type(friend)) == "<class 'int'>":   #specific friend
        for friendDB in Friends:
            if str(friend) == str(friendDB.ID):
                returned[friendDB.ID] = []
                duplicates = [item for item, count in collections.Counter(friendDB.TradingCards).items() if count > 1]
                duplicates = filterByGame(duplicates, games)
                if len(duplicates) > 0:
                    for dup in duplicates:
                        returned[friendDB.ID].append(dup)
    else:
        for friend_ in friend:
            temp = getDuplicates(friend_, games, True)
            returned[friend_] = []
            for dup in temp[friend_]:
                returned[friend_].append(dup)
    return returned

def getGamesFromDatabase():
    Games = []
    db, cur = basics.openDatabase(config.databasepath)
    sql = "SELECT * FROM Games"
    cur.execute(sql)
    test = cur.fetchall()
    for line in test:
        newGame = Game()
        newGame.Name = line[1]
        newGame.ID = line[0]
        Games.append(newGame)
    return Games
    
def getBoosterPackPrice(games): #this only works if you own the game
    if str(type(games)) == "<class 'str'>" or str(type(games)) == "<class 'int'>":
        games = [int(games)]
    userGames = getGamesFromDatabase()
    
    returned = {}
    
    for game in games:
        IDs, Names = [Game.ID for Game in userGames], [Game.Name for Game in userGames]
        try:    
            test = IDs.index(game)
        except:
            print("[Error] You don't own this game: " + str(game) + ". getBoosterPackPrice() can only be used with games you own.")
            return
        nameToSearch = Names[IDs.index(game)]
        
        url = "http://steamcommunity.com/market/listings/753/" + str(game) + "-" + nameToSearch + " Booster Pack/render"
        webpage = basics.downloadString(url, {"currency" : config.currency})
        
        print(url)
        
        loadedJSON = json.loads(webpage)
        
        try:
            for key in loadedJSON["listinginfo"].keys(): #The trick is, there is only one key, so it's the good one.
                price = loadedJSON["listinginfo"][key]["converted_price"]
            
            returned[userGames[IDs.index(game)]] = price
        except:
            returned[userGames[IDs.index(game)]] = -1
        
    return returned

# print(getBoosterPackPrice(730))
    
def findFriendsWithGames(games, mustMatchAllGames):
    if str(type(games)) == "<class 'str'>" or str(type(games)) == "<class 'int'>":
        games = [int(games)]
    returned = []
    Friends = getFriendsAsList()
    if mustMatchAllGames:
        for friend in Friends:
            ok = True
            for game in games:
                if not game in friend.Games:
                    ok = False
            if ok:
                returned.append(friend)
    else:
        for friend in Friends:
            for game in games:
                if game in friend.Games:
                    if not friend in returned:
                        returned.append(friend)
    return returned
       
def printResults(cardsDictionary, showEmpty):
    friendsDictionary = getFriendsAsDictionary()
    temp = [cardsDictionary[key] for key in cardsDictionary if cardsDictionary[key] != []]
    if len(temp) == 0:
        print("No results.")
    for user in cardsDictionary:
        if len(cardsDictionary[user]) > 0 or showEmpty:
            try:
                print(str(user) + " (" + friendsDictionary[user].Name + ")")
            except:
                print(str(user) + " (Undisplayable name)")
        for card in cardsDictionary[user]:
            print("\t" + str(card))
            
            
##############################################################################
#                             ADD YOUR CODE HERE                             #
# HELP HERE : https://bitbucket.org/ThomasKowalski/steam-trading-cards-tools #
##############################################################################

