AppIDs = {
    "Super Meat Boy" : 40800,
    "Monaco" : 113020,
    "PAYDAY 2" : 218620,
    "Chivalry: Medieval Warfare" : 219640,
    "Garry's Mod" : 4000,
    "2016 Picnic Summer Sale" : 480730,
    "CSGO" : 730,
    "Counter-Strike: Global Offensive" : 730,
    "Draw A Stickman Epic" : 248650,
    "PAYDAY The Heist" : 24240,
    "Mini metro" : 287980,
    "Civilization V" : 8930,
    "Don't Starve Together" : 322330,
}

def normalize(string):
    string = string.replace(":", "").replace(",", "").replace("'", "").replace('"', '')
    string = string.lower()
    while "  " in string:
        string = string.replace("  ", " ")
    string = string.replace(" ", "")
    string = string.strip()
    return string

def getAppID(App):
    global AppIDs
    '''
    Returns the app id of the game matching the App (Name) parameter
    Experimental.
    '''
    App = normalize(App)
    bestMatch = -1
    for AppID in AppIDs:
        AppID_ = normalize(AppID)
        if AppID_ == App:
            return AppIDs[AppID]
    raise ValueError('App not found')
    return -1